(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/login/login.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Inicio de Sesión\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!-- <ion-grid> -->\n    <!-- <ion-row class=\"ion-justify-content-center align-content-center\"> -->\n      <!-- <ion-card> -->\n        <!-- <ion-card-content> -->\n          <form #formulario=\"ngForm\" (ngSubmit)=\"onSubmitTemplate()\">\n            <ion-list>\n              <ion-item>\n                <ion-label position=\"floating\">Ingrese su Usuario</ion-label>\n                <ion-input\n                  required\n                  clearInput\n                  type=\"email\"\n                  name=\"email\"\n                  [(ngModel)]=\"usuario.email\"\n                  placeholder=\"usermm@mmm.mm\"\n                  value=\"usermm@mmm.mm\"\n                  pattern=\"^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$\"\n                ></ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-label position=\"floating\">Ingrese su Contraseña</ion-label>\n                <ion-input\n                  required\n                  clearInput\n                  type=\"password\"\n                  name=\"password\"\n                  [(ngModel)]=\"usuario.password\"\n                  placeholder=\"passmm\"\n                  value=\"passmm\"\n                ></ion-input>\n              </ion-item>\n            </ion-list>\n            <ion-grid *ngIf=\"error\"\n              ><ion-row justify-content-center align-items-center\n                >Datos Incorrectos Intente nuevamente</ion-row\n              ></ion-grid\n            >\n            <ion-button\n              expand=\"full\"\n              type=\"submit\"\n              [disabled]=\"formulario.invalid\"\n            >\n              Iniciar Sesión</ion-button\n            >\n          </form>\n          <ion-button expand=\"full\" routerLink=\"/register\">\n            Registrarse</ion-button\n          >\n          <!-- <ion-button expand=\"full\" (click)=\"probando()\"> Prueba</ion-button> -->\n        <!-- </ion-card-content> -->\n      <!-- </ion-card> -->\n    <!-- </ion-row> -->\n  <!-- </ion-grid> -->\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/variables.service */ "./src/app/services/variables.service.ts");
/* harmony import */ var _services_mysevice_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/mysevice.service */ "./src/app/services/mysevice.service.ts");


//nuevos






var LoginPage = /** @class */ (function () {
    function LoginPage(router, alertCtrl, http, myv, mys, st) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.myv = myv;
        this.mys = mys;
        this.st = st;
        this.usuario = { email: "", password: "" };
        // url='http://localhost:8081/confirm?user=user03&pass=pass03';
        this.urlbase = "http://localhost:8081/confirm?user=";
        this.urlpas = "&pass=";
        this.error = false;
        this.userSigned = false;
    }
    LoginPage.prototype.ngOnInit = function () { };
    LoginPage.prototype.onSubmitTemplate = function () {
        this.mys.confirmUser(this.usuario);
        try {
            // caso si el usuario se registro exitosamente
            // let usuario = this.myv.getUserSigned()
            var usuario = this.myv.isSetUserSigned();
            console.log(usuario);
            // if (usuario.phone) {
            //   console.log('uuuuuuuuuuuuu1');
            //   this.router.navigateByUrl('home')
            // } else{
            //   console.log('uuuuuuuuuuuuu2');
            // }
        }
        catch (error) {
            // caso si el usuario NO se registro
            console.log('Intente nuevamente.. Datos Incorrectos..');
            alert('Intente nuevamente.. Datos Incorrectos..');
        }
        // console.log(this.myv.getGlobal());
        // let variablex = this.myv.isUserSigned()
        // console.log(variablex);
        // console.log(this.myv.isUserSigned());
        // console.log(this.myv.getUserSigned());
        // console.log("ttttttttttttttttttttttttttttttttttt3");
        // if (this.myv.isUserSigned()) {
        //   this.router.navigateByUrl('home');
        // } else {
        //   this.router.navigateByUrl('login');
        // }
        // );
        // this.user = this.myv.getUserSigned();
        // this.userSigned = this.myv.isUserSigned();
        // if (this.userSigned) {
        // // if (this.userSigned) {
        //   // this.user = this.myv.getUserSigned();
        //   this.userSigned = true;
        //   console.log('wwwwwwwwwwwwwwwwwwww1');
        // } else {
        //   console.log('wwwwwwwwwwwwwwwwwwww4');
        //   this.user = '';
        //   this.userSigned = false;
        // }
        // let datox = window['mydata']
        // console.log(datox);
    };
    LoginPage.prototype.ionViewWillEnter = function () { };
    LoginPage.prototype.probando = function () {
        // console.log(this.myv.getGlobal());
        // this.myv.miVariable = this.myv.miVariable + '.Xx'
        // console.log(this.myv.miVariable);
    };
    LoginPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
        { type: _services_variables_service__WEBPACK_IMPORTED_MODULE_6__["VariablesService"] },
        { type: _services_mysevice_service__WEBPACK_IMPORTED_MODULE_7__["MyseviceService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] }
    ]; };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-login",
            template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"],
            _services_variables_service__WEBPACK_IMPORTED_MODULE_6__["VariablesService"],
            _services_mysevice_service__WEBPACK_IMPORTED_MODULE_7__["MyseviceService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es5.js.map