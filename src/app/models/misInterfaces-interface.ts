
export interface Inmueble {
    id: number;
    name: string;
    place: string;
    pics: Array<string>;
    price: number;
    type: string;
    areat: number;
    areac: number;
    bedroom: number;
    antiquity: number;
    bathroom: number;
    floors: number;
    parking: number;
    description: string;
    active: boolean;
}

export interface Cita {
    id: number;                 // id de la cita

    id_interesado: number;      // el que planifico la cita
    id_propietario: number;     // el dueno del inmueble
    id_corredor: number;        // el que aprobo la publicacion del inmueble

    // dia 1 //////////////////////////////////////////////////////////////////
    day_1: string;
    time_1_a_i: string;         // hora que planifico el interesado
    time_1_b_i: string;
    time_1_c_i: string;

    time_1_a_c: boolean;        // hora aprobada o NO por el Corredor
    time_1_b_c: boolean;
    time_1_c_c: boolean;

    time_1_a_p: boolean;        // hora aprobada o NO por el Propietario
    time_1_b_p: boolean;
    time_1_c_p: boolean;

    // dia 2 //////////////////////////////////////////////////////////////////
    day_2: string;
    time_2_a_i: string;
    time_2_b_i: string;
    time_2_c_i: string;

    time_2_a_c: boolean;
    time_2_b_c: boolean;
    time_2_c_c: boolean;

    time_2_a_p: boolean;
    time_2_b_p: boolean;
    time_2_c_p: boolean;

    // dia 3 //////////////////////////////////////////////////////////////////
    day_3: string;
    time_3_a_i: string;
    time_3_b_i: string;
    time_3_c_i: string;

    time_3_a_c: boolean;
    time_3_b_c: boolean;
    time_3_c_c: boolean;

    time_3_a_p: boolean;
    time_3_b_p: boolean;
    time_3_c_p: boolean;
}

export interface User {
    id: number;
    user: string;
    pass: string;
    rol: string;
    phone: string;
    active: boolean;
}
