import { Component, OnInit, OnDestroy } from '@angular/core';
// nuevos
import { Router, PRIMARY_OUTLET } from '@angular/router';
import {
  AlertController,
  ActionSheetController,
  LoadingController
} from '@ionic/angular';
import { MyseviceService } from '../services/mysevice.service';
import { MenuController } from '@ionic/angular';
// import { Storage } from '@ionic/storage';
import { VariablesService } from '../services/variables.service';
// import { ThrowStmt } from '@angular/compiler';
// import { ActionSheetController } from '@ionic/angular';*****
import { Platform } from '@ionic/angular';
import { Location } from '@angular/common';
import { present } from '@ionic/core/dist/types/utils/overlays';

//
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  constructor(
    private router: Router,
    private alertController: AlertController,
    private mys: MyseviceService, // private http: HttpClient
    private myv: VariablesService,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public location: Location,
    public menuController: MenuController
  ) { }


  public unsubscribeBackEvent: any; // variable para boton back
  alertSalir = false;
  actionSheetVisible = false;

  actionSheet;

  loading: any;
  user: any;
  userSigned = false;
  email: string;
  inmuebles: any;
  urlimage = this.mys.UrlBase + '/Images/Inmuebles/';

  // urlimage = this.mys.urlBase + '/Images/Inmuebles/';

  ngOnInit() {
    console.log('ngOnInit desde home solo la primera ves...');
    this.initializeBackButtonCustomHandler(); // para capturar boton back
  }

  ionViewWillEnter() {
    this.inmuebles = this.myv.getInmuebles();
    this.user = this.myv.getUserSigned();
    this.userSigned = this.myv.isUserSigned();
    if (this.userSigned) {
      this.user = this.myv.getUserSigned();
      this.userSigned = true;
    } else {
      this.user = '';
      this.userSigned = false;
    }
  }

  cerrarsesion() { }

  detalleinmueble(ninmueble: number) {
    const inmuebles = this.myv.getInmuebleDetailId(ninmueble);
    this.myv.setInmueble(inmuebles);
    this.router.navigateByUrl('house-detail');
  }

  prueba() { }

  ionViewDidEnter() {
    if (this.mys.isLoading) {
      this.mys.loadingDismiss();
    }
  }

  async alanzarMenu() {
    this.actionSheet = await this.actionSheetCtrl.create({
      backdropDismiss: true,
      buttons: [
        {
          text: 'Compartir en Redes',
          icon: 'share',
          cssClass: 'actionSheet-Global-inmueble',
          handler: () => {
            console.log('Se pulsó en Compartir');
          }
        },
        {
          text: 'Añadir a Favorito',
          icon: 'star',
          cssClass: 'actionSheet-Global-inmueble',
          handler: () => {
            console.log('Se pulsó en Añadir a Favorito');
          }
        }
      ]
    });
    this.actionSheetVisible = true;
    this.actionSheet.present();
  }

  initializeBackButtonCustomHandler(): void {
    this.unsubscribeBackEvent = this.platform.backButton.subscribeWithPriority(
      999999,
      () => {
        this.menuController.isOpen().then(ret => {
          if (ret) {
            // si esta abierto el menu lo cierro
            this.menuController.close('mymenu');
          } else {
            // si NO esta abierto el menu
            if (this.actionSheetVisible) {
              this.actionSheetVisible = false;
              this.actionSheet.dismiss();
            } else {
              if (this.router.url === '/home') {
                if (!this.alertSalir) {
                  this.alertSalir = !this.alertSalir;
                  this.presentAlertConfirm();
                }
              } else {
                this.location.back();
              }
            }
          }
        });
      }
    );
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Desea Cerrar la Aplicación?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {
            console.log('Volviendo a la App  ' + this.constructor.name);
            this.alertSalir = !this.alertSalir;
          }
        },
        {
          text: 'SI',
          handler: () => {
            console.log('Cerrando la App');
            this.alertSalir = !this.alertSalir;
            navigator['app'].exitApp();
          }
        }
      ]
    });

    await alert.present();
  }

  OnDestroy() {
    this.unsubscribeBackEvent && this.unsubscribeBackEvent();
    console.log('fffffffffffffffffffhhhhhhhhhhhhhhhhhh');
  }




}
