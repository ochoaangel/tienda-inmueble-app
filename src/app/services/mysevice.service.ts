import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { VariablesService } from '../services/variables.service';
import { User, Cita, Inmueble } from '../models/misInterfaces-interface';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MyseviceService {

  constructor(
    private httpClient: HttpClient,
    private st: Storage,
    private myv: VariablesService,
    public loadingCtrl: LoadingController,
    public toastcontroller: ToastController,

  ) { }


  public isLoading = false;
  // variables ///////////////////
  // UrlBase = 'http://192.168.16.106:8081';
  UrlBase = 'http://192.168.16.113:8081';
  VarGlobal = 'mydata'; // en  el index
  usuario = { email: '', password: '' };
  urlConfirm = '/confirm?user=';
  urlPass = '&pass=';

  async loadingPresent() {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      message: 'Cargando ...',
      spinner: 'circles'
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort laoding'));
        }
      });
    });
  }

  async loadingDismiss() {
    this.isLoading = false;
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  ////////////////////////////////////////////////////////////////////
  //////////////////////////// API ///////////////////////////////////
  ////////////////////////////////////////////////////////////////////
  public getinmuebles(): void {
    // guardo los inmuebles en storage
    this.httpClient
      .get(this.UrlBase + '/inmuebles')
      .subscribe(data => {
        this.st.set('inmuebles', data);
        console.log('Guardado inmuebles desde Services ' + data);
      });
  }

  ////////////////////////////////////////////////////////////////////
  public usersigned(): any {
    this.st.get('user').then(valor => {
      if (valor) {
        console.log('Usuario registrado - desde service');
        return true;
      } else {
        console.log('Usuario NO registrado - desde service');
        return false;
      }
    });
  }

  public getemail(): any {
    this.st.get('user').then(valor => {
      if (valor) {
        return valor[0].user;
      } else {
        console.log(valor);
        return 'no hay acceso';
      }
    });
  }

  public getrol(): any {
    this.st.get('user').then(valor => {
      if (valor) {
        return valor[0].rol;
      } else {
        // console.log(valor);
        return 'no hay acceso';
      }
    });
  }

  ////////////////////////////////////////////////////////////////////
  //////////////////////////// GENERAL ///////////////////////////////
  public getAndSetInmuebles(): void {
    let urlFinal = this.UrlBase + '/inmuebles';
    console.log('Getting la lista de inmuebles desde: ' + urlFinal);
    this.httpClient.get(urlFinal).subscribe(data => {
      // console.log(data);
      this.myv.setInmuebles(data);
      // this.myv.setInmuebles(data)

    });
  }

  public confirmUser(user: any): void {
    let urlFinal =
      this.UrlBase +
      this.urlConfirm +
      user.email +
      this.urlPass +
      user.password;
    console.log('Confirmando usuario con : ' + urlFinal);

    this.httpClient.get(urlFinal).subscribe(data => {
      try {
        // caso si el usuario SI se Logeo
        if (data[0].phone) {
          this.myv.setUserSigned(data[0]);
          console.log('El usuario se logeo satisfactoriamente desde myService.');
        }
      } catch (error) {
        // caso si el usuario NO se Logeo
        console.log('El usuario NO se logeo desde myService.');
      }
    });
  }


  ////////////////////////////////////////////////////////////////////////
  async toast_mostrar(message: string) {
    const toast = await this.toastcontroller.create({
      message,
      animated: true,
      color: 'primary',
      duration: 2000
    });
    toast.present();
  }
  ////////////////////////////////////////////////////////////////////////





} /////////// fin my service
