import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { MyseviceService } from './services/mysevice.service';
import { VariablesService } from './services/variables.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  // user: any;
  // user = window['user'];
  btniniciar = true;
  email = 'Usuario';

  ngOnInit() {
    console.log('ngOnInit desde app.components solo la primera vez...');
    // this.mys.loadingPresent();
    console.log('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');



    const ururur = this.myv.getGlobal();
    // console.log(ururur);
    this.mys.getAndSetInmuebles();
    // let hfhfhfh = this.myv.getGlobal()
    // console.log(hfhfhfh);
    // console.log(hfhfhfh.lenght);
    // this.myv.getAndSetInmuebles()

    // if (this.mys.isSetInmuebles()) {
    //   console.log('sssssssssssi hay inmuebles');
    // } else {
    //   console.log('nnnnnnnnnnnnnno hay inmuebles');
    // }

    console.log('cargando inmuebeles en Global');
  }
  // tslint:disable-next-line: member-ordering
  public appPages = [
    {
      title: 'Buscar Inmueble',
      url: '/search',
      icon: 'search'
    },
    {
      title: 'Catalogo de Inmuebles',
      url: '/home',
      icon: 'images'
    },
    {
      title: 'Favoritos',
      url: '/favorites',
      icon: 'heart'
    },
    {
      title: 'Visitas Propuestas',
      url: '/view-open',
      icon: 'copy'
    },
    {
      title: 'Visitas en Espera',
      url: '/view-wait',
      icon: 'clock'
    },
    {
      title: 'Visitas en Confirmadas',
      url: '/view-close',
      icon: 'checkmark-circle'
    },
    {
      title: 'Publicar Inmueble',
      url: '/new-house',
      icon: 'cloud-upload'
    },
    {
      title: 'Configuración',
      url: '/configuration',
      icon: 'construct'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public http: HttpClient,
    private st: Storage,
    private mys: MyseviceService, // private http: HttpClient
    private myv: VariablesService, // private http: HttpClient
    private menuController: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    // this.mys.loadingPresent();
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }


  // onClick() {
  //   console.log('ffffffffffffffffffffffffffffffffffffffffffffff');

  //   this.menuController.isOpen().then(ret => {
  //     if (ret) { this.menuController.close('mymenu'); } else {
  //       // caso de salir
  //     }
  //   });



    
  // }


}
