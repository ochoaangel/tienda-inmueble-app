import { Pipe, PipeTransform } from '@angular/core';
// import { Title } from '@angular/platform-browser';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(
    arreglo: any[],
    texto: string,
    columna: string,
  ): any[] {

    if (texto === '') { return arreglo; }        /// para la lista completa mientras no haya busqueda
    // if (texto === '') { return []; }              /// para la lista vacía mientras no haya busqueda

    texto = texto.toLowerCase();

    return arreglo.filter(item => {
      return item[columna].toLowerCase().includes(texto);
    });
  }

}
