import { Component, OnInit } from '@angular/core';
// nuevos
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MyseviceService } from '../../services/mysevice.service';
import { VariablesService } from '../../services/variables.service';

@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.page.html',
  styleUrls: ['./house-detail.page.scss']
})
export class HouseDetailPage implements OnInit {
  rol: string;
  user: string;
  inmueble: any;
  public prueba = 'varhbthathasrtjjts';
  public titulox = 'mititulo';
  public urlimage = this.mys.UrlBase + '/Images/Inmuebles/';

  constructor(
    private router: Router,
    private alertCtrl: AlertController,
    private st: Storage,
    private mys: MyseviceService,
    private myv: VariablesService
  ) { }

  ngOnInit() {
    if (this.myv.isSetInmueble()) {
      this.inmueble = this.myv.getInmueble();
    } else {
      this.router.navigateByUrl('home');
    }
  }

  planificarvisita() {
    if (this.myv.isUserSigned()) {
      this.router.navigateByUrl('create-visit');
    } else {
      this.router.navigateByUrl('login');
    }

    // this.st.get('user').then(valor => {
    //   if (valor) {
    //     this.router.navigateByUrl('create-visit');
    //   } else {
    //     this.router.navigateByUrl('login');
    //   }
    // });
  } // fin planificarvisita

  ionViewWillEnter() { }

  ionViewDidEnter() { }

  ionViewDidLeave() {
    this.myv.setRemoveInmueble();
  }
} // fin clase
