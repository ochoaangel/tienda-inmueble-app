import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  onClick() {
    console.log(moment().format());
    console.log(moment('2015-10-20').isBefore('2015-10-19'));
  }
}
