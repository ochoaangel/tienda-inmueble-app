(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-house-detail-house-detail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/house-detail/house-detail.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/house-detail/house-detail.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title > Detalle de Inmueble </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content justify-content-center align-items-center> \n  <ion-card>\n    <ion-img  src=\"{{ this.urlimage + this.inmueble.pics[0] + '.jpg' }}\">\n    </ion-img>\n  </ion-card>\n\n  <ion-grid fixed style=\"margin-top: -15px\">\n    <ion-row justify-content-center align-items-center>\n      <ion-text color=\"secondary\">\n        <h1>\n          <strong >{{ inmueble.name }}</strong>\n        </h1>\n      </ion-text>\n    </ion-row>\n  </ion-grid>\n\n  <div style=\"height: 50px;background-color: rgb(238, 238, 238);border-radius: 20px; margin-left: 10%; margin-right: 10% ;margin-top: 5px \">\n    <div style=\"width: 50%; height: 100%; text-align: center;float:left;\">\n      <ion-text color=\"secondary\">\n        <h4 style=\"line-height: 15px\">\n          <strong>Precio</strong>\n        </h4>\n      </ion-text>\n    </div>\n    <div\n      style=\"width: 50%; height: 100%; background-color: lightgrey ; float:right; border-radius: 20px;; text-align: center; margin-top: 0px\">\n      <ion-text color=\"light\">\n        <h2 style=\"line-height: 8px\">\n          <strong>{{ this.inmueble.price }} $</strong>\n        </h2>\n      </ion-text>\n    </div>\n  </div>\n\n  <!-- <ion-card>\n    <ion-grid>\n      <ion-label>Precio:</ion-label>\n      <ion-row justify-content-center align-items-center>\n        <ion-label>\n          <h1>{{ this.inmueble.price }} $</h1>\n        </ion-label>\n      </ion-row>\n    </ion-grid>\n  </ion-card> -->\n\n  <ion-grid fixed style=\"margin-top: 20px;\">\n    <ion-row>\n      <ion-text color=\"secondary\">\n        {{ inmueble.place }}\n      </ion-text>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid fixed style=\"margin-top: 10px ; margin-bottom: 10px\">\n    <ion-row>\n      <ion-text color=\"secondary\">\n        {{ inmueble.description }}\n      </ion-text>\n    </ion-row>\n  </ion-grid>\n\n  <!-- Features and Amenities -->\n  <ion-item-group>\n    <!-- Features -->\n    <div style=\"height: fit-content;  background-color: rgb(238, 238, 238);padding-bottom: 30px;\">\n      <ion-grid fixed>\n        <ion-row justify-content-center align-items-center>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Área construida</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.areac\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item class=\"ion-padding-\">\n              <ion-label style=\"color: #72726e\">Área terreno</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.areat\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Parking</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.parking\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Baños</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.bathroom\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Antiguedad</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.antiquity\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Pisos</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.floors\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n        </ion-row>\n      </ion-grid>\n    </div>\n\n    <!-- Amenities -->\n    <ion-grid fixed>\n      <ion-row justify-content-center align-items-center>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Amoblada</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"success\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Wifi</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"success\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Jardin</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"success\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Piscina</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"success\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Salón de juegos</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"success\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Vigilancia privada</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"success\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Circuito cerrado</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"success\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-item-group>\n</ion-content>\n\n<!-- <ion-button shape=\"round\" color=\"secondary\" (click)=\"planificarvisita()\" style=\"text-transform: none;\">\n  <ion-text color=\"light\">Planificar Visita</ion-text>\n</ion-button> -->\n\n<div style=\"text-align: center; height: fit-content;\">\n    <ion-button shape=\"round\" color=\"secondary\" (click)=\"planificarvisita()\" style=\"text-transform: none;\">\n      <ion-text color=\"light\">Planificar Visita</ion-text>\n    </ion-button>\n  </div>"

/***/ }),

/***/ "./src/app/pages/house-detail/house-detail.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/house-detail/house-detail.module.ts ***!
  \***********************************************************/
/*! exports provided: HouseDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseDetailPageModule", function() { return HouseDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _house_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./house-detail.page */ "./src/app/pages/house-detail/house-detail.page.ts");







var routes = [
    {
        path: '',
        component: _house_detail_page__WEBPACK_IMPORTED_MODULE_6__["HouseDetailPage"]
    }
];
var HouseDetailPageModule = /** @class */ (function () {
    function HouseDetailPageModule() {
    }
    HouseDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            ],
            declarations: [_house_detail_page__WEBPACK_IMPORTED_MODULE_6__["HouseDetailPage"]]
        })
    ], HouseDetailPageModule);
    return HouseDetailPageModule;
}());



/***/ }),

/***/ "./src/app/pages/house-detail/house-detail.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/house-detail/house-detail.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-item {\n  --ion-background-color: transparent !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG91c2UtZGV0YWlsL0M6XFxVc2Vyc1xcYXBwXFxEb2N1bWVudHNcXC0tbWlzUHJveWVjdG9zXFx0aWVuZGEtaW5tdWVibGUtYXBwL3NyY1xcYXBwXFxwYWdlc1xcaG91c2UtZGV0YWlsXFxob3VzZS1kZXRhaWwucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9ob3VzZS1kZXRhaWwvaG91c2UtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFXQTtFQUVFLDhDQUFBO0FDWEYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob3VzZS1kZXRhaWwvaG91c2UtZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGlvbi1pdGVtIHtcclxuLy8gLS1ib3JkZXItY29sb3I6IFwiZG9yYWRvXCI7XHJcbi8vIC0tcGFkZGluZy1ib3R0b206IDBweDtcclxuLy8gLS1wYWRkaW5nLWVuZDogMHB4O1xyXG4vLyAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuLy8gLS1wYWRkaW5nLWVuZDogMHB4O1xyXG4vLyB9XHJcblxyXG4vLyAubGFiZWx7XHJcbi8vICAgY29sb3I6ICM3MjcyNmVcIlxyXG4vLyB9XHJcbmlvbi1pdGVte1xyXG4gIC8vIC0taW9uLWJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JleTtcclxuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG59IiwiaW9uLWl0ZW0ge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/house-detail/house-detail.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/house-detail/house-detail.page.ts ***!
  \*********************************************************/
/*! exports provided: HouseDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseDetailPage", function() { return HouseDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _services_mysevice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/mysevice.service */ "./src/app/services/mysevice.service.ts");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/variables.service */ "./src/app/services/variables.service.ts");


// nuevos





var HouseDetailPage = /** @class */ (function () {
    function HouseDetailPage(router, alertCtrl, st, mys, myv) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.st = st;
        this.mys = mys;
        this.myv = myv;
        this.prueba = 'varhbthathasrtjjts';
        this.titulox = 'mititulo';
        this.urlimage = this.mys.UrlBase + '/Images/Inmuebles/';
    }
    HouseDetailPage.prototype.ngOnInit = function () {
        if (this.myv.isSetInmueble()) {
            this.inmueble = this.myv.getInmueble();
        }
        else {
            this.router.navigateByUrl('home');
        }
    };
    HouseDetailPage.prototype.planificarvisita = function () {
        if (this.myv.isUserSigned()) {
            this.router.navigateByUrl('create-visit');
        }
        else {
            this.router.navigateByUrl('login');
        }
        // this.st.get('user').then(valor => {
        //   if (valor) {
        //     this.router.navigateByUrl('create-visit');
        //   } else {
        //     this.router.navigateByUrl('login');
        //   }
        // });
    }; // fin planificarvisita
    HouseDetailPage.prototype.ionViewWillEnter = function () { };
    HouseDetailPage.prototype.ionViewDidEnter = function () { };
    HouseDetailPage.prototype.ionViewDidLeave = function () {
        this.myv.setRemoveInmueble();
    };
    HouseDetailPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
        { type: _services_mysevice_service__WEBPACK_IMPORTED_MODULE_5__["MyseviceService"] },
        { type: _services_variables_service__WEBPACK_IMPORTED_MODULE_6__["VariablesService"] }
    ]; };
    HouseDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-house-detail',
            template: __webpack_require__(/*! raw-loader!./house-detail.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/house-detail/house-detail.page.html"),
            styles: [__webpack_require__(/*! ./house-detail.page.scss */ "./src/app/pages/house-detail/house-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
            _services_mysevice_service__WEBPACK_IMPORTED_MODULE_5__["MyseviceService"],
            _services_variables_service__WEBPACK_IMPORTED_MODULE_6__["VariablesService"]])
    ], HouseDetailPage);
    return HouseDetailPage;
}()); // fin clase



/***/ })

}]);
//# sourceMappingURL=pages-house-detail-house-detail-module-es5.js.map