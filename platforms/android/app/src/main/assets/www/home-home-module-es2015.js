(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button> </ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n        <!-- class=\"animated rubberBand infinite slower\" -->\n      <!-- <ion-img src='../../assets/myimages/header.jpg' style=\"height: 25px;\" class=\"ion-no-padding\" class=\"ion-no-padding\" ></ion-img> -->\n      <ion-img  \n            \n      src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-button shape=\"round\" color=\"success\" routerLink=\"/login\" *ngIf=\"!this.userSigned\" style=\"text-transform: none;\"\n  routerLink=\"/login\">\n  <ion-text color=\"light\">\n    Inicio de sesión\n  </ion-text>\n</ion-button>\n\n<!-- <ion-button shape=\"round\" color=\"success\" style=\"text-transform: none;\" (click)=\"pruebax()\">\n  <ion-text color=\"light\">\n    Prueba\n  </ion-text>\n</ion-button> -->\n\n<!-- <div >\n  <div style=\"width:50%; display:inline-table\">Precio</div>\n  <div style=\"width:50%; display:inline-table\">Valor</div>\n</div> -->\n\n<!-- <ion-img src=\"../../assets/myimages/gif.gif\" ></ion-img> -->\n\n<!-- <ion-button expand=\"full\" routerLink=\"/login\" *ngIf=\"!this.userSigned\">\n  Bienvenido... Inicia Sesión..</ion-button\n> -->\n<!-- <a href=\"whatsapp://send?phone=584242332373?text=Me%20gustaría%20saber%20el%20precio%20del%20coche\">Link WhatsApp</a> -->\n\n<ion-content>\n  \n  <ion-row class=\"ion-justify-content-center\">\n    <ion-label *ngIf=\"this.userSigned\">Bienvenido {{ this.user.email }}</ion-label>\n    <ion-button expand=\"full\" *ngIf=\"this.userSigned\" (click)=\"cerrarsesion()\">Cierra sesión</ion-button>\n  </ion-row>\n\n<!-- \n  <ion-img class=\"animated infinite bounce delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flash delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite pulse delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rubberBand delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite shake delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite headShake delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite swing delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite tada delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite wobble delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite jello delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceInDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceInLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceInRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceInUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOutDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOutLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOutRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOutUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInDownBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInLeftBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInRightBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInUpBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutDownBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutLeftBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutRightBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutUpBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flipInX delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flipInY delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flipOutX delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flipOutY delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite lightSpeedIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite lightSpeedOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateInDownLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateInDownRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateInUpLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateInUpRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOutDownLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOutDownRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOutUpLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOutUpRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite hinge delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite jackInTheBox delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rollIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rollOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomInDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomInLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomInRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomInUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOutDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOutLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOutRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOutUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideInDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideInLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideInRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideInUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideOutDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideOutLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideOutRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideOutUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite heartBeat delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img> -->\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n  <!-- ooooooooooooooooooooooooooooooooBUCLEoooooooooooooooooooooooooooooooo -->\n  <ion-card class=\"welcome-card\" *ngFor=\"let inmueble of inmuebles\">\n    <img src=\"{{ urlimage + inmueble.pics[0] + '.jpg' }}\" (click)=\"detalleinmueble(inmueble.id)\" />\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"10\" (click)=\"detalleinmueble(inmueble.id)\">\n          <ion-card-title> <ion-text color=\"secondary\">\n            <strong>{{ inmueble.name }}</strong>\n          </ion-text></ion-card-title>\n        </ion-col>\n        <ion-col>\n          <!-- <ion-icon name=\"play\"></ion-icon> -->\n          <div class=\"inmueble-more\">\n            <ion-button fill=\"clear\" (click)=\"alanzarMenu()\" style=\"height: 100%\">\n              <ion-icon slot=\"icon-only\" name=\"more\" color=\"secondary\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <!-- ooooooooooooooooooooooooooooooooFIN BUCLEoooooooooooooooooooooooooooooooo -->\n</ion-content>"

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: "",
                    component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                }
            ]),
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\n.inmueble-more {\n  font-size: 25px !important;\n  position: absolute;\n  right: -10px;\n  top: -10px;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXGFwcFxcRG9jdW1lbnRzXFwtLW1pc1Byb3llY3Rvc1xcdGllbmRhLWlubXVlYmxlLWFwcC9zcmNcXGFwcFxcaG9tZVxcaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUE7RUFDRSwwQkFBQTtFQUNBLGtCQUFBO0VBRUEsWUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FDQUYiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUtY2FyZCBpbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uaW5tdWVibGUtbW9yZSB7XG4gIGZvbnQtc2l6ZTogMjVweCAhaW1wb3J0YW50O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8vIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcmlnaHQ6IC0xMHB4O1xuICB0b3A6IC0xMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG5cbiAgLy8gYm90dG9tOiAtNXB4O1xufVxuIiwiLndlbGNvbWUtY2FyZCBpbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uaW5tdWVibGUtbW9yZSB7XG4gIGZvbnQtc2l6ZTogMjVweCAhaW1wb3J0YW50O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAtMTBweDtcbiAgdG9wOiAtMTBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_mysevice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/mysevice.service */ "./src/app/services/mysevice.service.ts");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/variables.service */ "./src/app/services/variables.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");


// nuevos




// import { Storage } from '@ionic/storage';

// import { ThrowStmt } from '@angular/compiler';
// import { ActionSheetController } from '@ionic/angular';*****


//
let HomePage = class HomePage {
    constructor(router, alertController, mys, // private http: HttpClient
    myv, actionSheetCtrl, loadingCtrl, platform, location, menuController) {
        this.router = router;
        this.alertController = alertController;
        this.mys = mys;
        this.myv = myv;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.location = location;
        this.menuController = menuController;
        this.alertSalir = false;
        this.actionSheetVisible = false;
        this.userSigned = false;
        this.urlimage = this.mys.UrlBase + '/Images/Inmuebles/';
    }
    // urlimage = this.mys.urlBase + '/Images/Inmuebles/';
    ngOnInit() {
        console.log('ngOnInit desde home solo la primera ves...');
        this.initializeBackButtonCustomHandler(); // para capturar boton back
    }
    ionViewWillEnter() {
        this.inmuebles = this.myv.getInmuebles();
        this.user = this.myv.getUserSigned();
        this.userSigned = this.myv.isUserSigned();
        if (this.userSigned) {
            this.user = this.myv.getUserSigned();
            this.userSigned = true;
        }
        else {
            this.user = '';
            this.userSigned = false;
        }
    }
    cerrarsesion() { }
    detalleinmueble(ninmueble) {
        const inmuebles = this.myv.getInmuebleDetailId(ninmueble);
        this.myv.setInmueble(inmuebles);
        this.router.navigateByUrl('house-detail');
    }
    prueba() { }
    ionViewDidEnter() {
        if (this.mys.isLoading) {
            this.mys.loadingDismiss();
        }
    }
    alanzarMenu() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.actionSheet = yield this.actionSheetCtrl.create({
                backdropDismiss: true,
                buttons: [
                    {
                        text: 'Compartir en Redes',
                        icon: 'share',
                        cssClass: 'actionSheet-Global-inmueble',
                        handler: () => {
                            console.log('Se pulsó en Compartir');
                        }
                    },
                    {
                        text: 'Añadir a Favorito',
                        icon: 'star',
                        cssClass: 'actionSheet-Global-inmueble',
                        handler: () => {
                            console.log('Se pulsó en Añadir a Favorito');
                        }
                    }
                ]
            });
            this.actionSheetVisible = true;
            this.actionSheet.present();
        });
    }
    initializeBackButtonCustomHandler() {
        this.unsubscribeBackEvent = this.platform.backButton.subscribeWithPriority(999999, () => {
            this.menuController.isOpen().then(ret => {
                if (ret) {
                    // si esta abierto el menu lo cierro
                    this.menuController.close('mymenu');
                }
                else {
                    // si NO esta abierto el menu
                    if (this.actionSheetVisible) {
                        this.actionSheetVisible = false;
                        this.actionSheet.dismiss();
                    }
                    else {
                        if (this.router.url === '/home') {
                            if (!this.alertSalir) {
                                this.alertSalir = !this.alertSalir;
                                this.presentAlertConfirm();
                            }
                        }
                        else {
                            this.location.back();
                        }
                    }
                }
            });
        });
    }
    presentAlertConfirm() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Desea Cerrar la Aplicación?',
                buttons: [
                    {
                        text: 'NO',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: blah => {
                            console.log('Volviendo a la App  ' + this.constructor.name);
                            this.alertSalir = !this.alertSalir;
                        }
                    },
                    {
                        text: 'SI',
                        handler: () => {
                            console.log('Cerrando la App');
                            this.alertSalir = !this.alertSalir;
                            navigator['app'].exitApp();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    OnDestroy() {
        this.unsubscribeBackEvent && this.unsubscribeBackEvent();
        console.log('fffffffffffffffffffhhhhhhhhhhhhhhhhhh');
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _services_mysevice_service__WEBPACK_IMPORTED_MODULE_4__["MyseviceService"] },
    { type: _services_variables_service__WEBPACK_IMPORTED_MODULE_5__["VariablesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html"),
        styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _services_mysevice_service__WEBPACK_IMPORTED_MODULE_4__["MyseviceService"],
        _services_variables_service__WEBPACK_IMPORTED_MODULE_5__["VariablesService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map