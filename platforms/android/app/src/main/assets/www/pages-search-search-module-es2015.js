(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-search-search-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/search/search.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/search/search.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n\n    <ion-title>Buscar Inmueble</ion-title>\n  </ion-toolbar>\n\n  <ion-searchbar\n    animated\n    placeholder=\"Buscar Inmueble Aquí\"\n    (ionChange)=\"buscar($event)\"\n  >\n  </ion-searchbar>\n</ion-header>\n<ion-content>\n  <ion-list>\n\n    <ion-item *ngFor=\"let album of albumes | filtro: textoBuscar:'title'\" (click)=\"onClickItem(1)\">\n    <ion-grid>\n        <ion-row>\n          <ion-col size=\"11\"  class=\"ion-align-self-center\"> {{ album.title }} </ion-col>\n          <ion-col size=\"1\"  class=\"ion-align-self-center\"><ion-icon name=\"play\"></ion-icon></ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-list>\n\n\n\n\n  <!-- <ion-list>\n    <ion-item *ngFor=\"let album of albumes | filtro: textoBuscar:'title'\">\n      {{ album.title }} \n    </ion-item>\n  </ion-list> -->\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/search/search.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/search/search.module.ts ***!
  \***********************************************/
/*! exports provided: SearchPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _search_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search.page */ "./src/app/pages/search/search.page.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");








const routes = [
    {
        path: '',
        component: _search_page__WEBPACK_IMPORTED_MODULE_6__["SearchPage"]
    }
];
let SearchPageModule = class SearchPageModule {
};
SearchPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_search_page__WEBPACK_IMPORTED_MODULE_6__["SearchPage"]]
    })
], SearchPageModule);



/***/ }),

/***/ "./src/app/pages/search/search.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/search/search.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NlYXJjaC9zZWFyY2gucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/search/search.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/search/search.page.ts ***!
  \*********************************************/
/*! exports provided: SearchPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPage", function() { return SearchPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/variables.service */ "./src/app/services/variables.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let SearchPage = class SearchPage {
    constructor(myv, router) {
        this.myv = myv;
        this.router = router;
        this.albumes = [];
        this.textoBuscar = '';
    }
    ngOnInit() {
        this.albumes = [
            { 'title': 'Casa1 ' },
            { 'title': 'Casa2 ' },
            { 'title': 'Casa3 ' },
            { 'title': 'Apt1 ' },
            { 'title': 'Apt2 ' },
            { 'title': 'Apt3 ' },
            { 'title': 'Terreno1 ' },
            { 'title': 'Terreno2 ' },
            { 'title': 'Terreno3 ' },
            { 'title': 'Apartamento1 ' },
            { 'title': 'Apartamento2 ' },
            { 'title': 'Apartamento3 ' },
            { 'title': 'Casa1 con patio ' },
            { 'title': 'Casa2 con patio ' },
            { 'title': 'Casa3 con patio ' },
            { 'title': 'Apt1 en avenida ' },
            { 'title': 'Apt2 en avenida ' },
            { 'title': 'Apt3 en avenida ' },
            { 'title': 'Terreno1 con bosque ' },
            { 'title': 'Terreno2 con bosque ' },
            { 'title': 'Terreno3 con bosque ' },
            { 'title': 'Apartamento1 para familia ' },
            { 'title': 'Apartamento2 para familia ' },
            { 'title': 'Apartamento3 para familia ' }
        ];
    }
    buscar(event) {
        this.textoBuscar = event.detail.value;
    }
    onClickItem(ninmueble) {
        let inmuebles = this.myv.getInmuebleDetailId(ninmueble);
        this.myv.setInmueble(inmuebles);
        this.router.navigateByUrl('house-detail');
    }
};
SearchPage.ctorParameters = () => [
    { type: _services_variables_service__WEBPACK_IMPORTED_MODULE_2__["VariablesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
SearchPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-search',
        template: __webpack_require__(/*! raw-loader!./search.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/search/search.page.html"),
        styles: [__webpack_require__(/*! ./search.page.scss */ "./src/app/pages/search/search.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_variables_service__WEBPACK_IMPORTED_MODULE_2__["VariablesService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], SearchPage);



/***/ })

}]);
//# sourceMappingURL=pages-search-search-module-es2015.js.map